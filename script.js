
$("button").on("click", function(){
    makeSound(this.innerHTML);
    tog(this.innerHTML);
})


$(document).on("keypress", function(event) {
        makeSound(event.key);
        tog(event.key);
})

function makeSound(c) {
    switch (c) {
        case "q":
            let sart1 = new Audio("sounds/sart1.mp3");
            sart1.play();
            break;
        case 's':
            let sart2 = new Audio("sounds/sart2.mp3");
            sart2.play();
            break;
        case 'd':
            let sart3 = new Audio("sounds/sart3.mp3");
            sart3.play();
            break;
        case 'f':
            let sart4 = new Audio("sounds/sart4.mp3");
            sart4.play();
            break;
        case 'g':
            let sart5 = new Audio("sounds/sart5.mp3");
            sart5.play();
            break;
        case 'h':
            let sart6 = new Audio("sounds/sart6.mp3");
            sart6.play();
            break;
        case 'j':
            let sart7 = new Audio("sounds/sart7.mp3");
            sart7.play();
            break;
        case "r":
            let sart8 = new Audio("sounds/sart8.mp3");
            sart8.play();
            break;
        case 't':
            let sart9 = new Audio("sounds/sart9.mp3");
            sart9.play();
            break;
        case 'y':
            let sart10 = new Audio("sounds/sart10.mp3");
            sart10.play();
            break;
        case 'u':
            let sart11 = new Audio("sounds/sart11.mp3");
            sart11.play();
            break;
        case 'i':
            let sart12 = new Audio("sounds/sart12.mp3");
            sart12.play();
            break;
        case 'o':
            let sart13 = new Audio("sounds/sart13.mp3");
            sart13.play();
            break;
        case 'p':
            let sart14 = new Audio("sounds/sart14.mp3");
            sart14.play();
            break;
    
        default:
            console.log("This is a sound");
            break;
    }
}

function tog(key) {
    document.querySelector("." + key).classList.add("pressed");
    setTimeout(function() {
        document.querySelector("." + key).classList.remove("pressed");    
    }, 100);
}